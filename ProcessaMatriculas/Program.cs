﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Configuration;

namespace ProcessaMatriculas
{
    public class Program
    {
        /*
         * Constantes de uso geral para a classe.
         */
        static Int32[] MULTIPLICADORES = new Int32[] { 5, 4, 3, 2 };
        static int DIVISOR = 16;

        static string DIGITO_SEPARADOR = "-";
        static string VERIFICACAO_SEPARADOR = " ";
        static string TRUE = "verdadeiro";
        static string FALSE = "falso";

        static string KEY_COMPLETAR = "C";
        static string KEY_VERIFICAR = "V";

        static string MATRICULA_INVALIDA = "matricula invalida";

        /*
         * Encapsulamento das constantes para uso nos testes unitários.
         */
        public static string TEST_DIGITO_SEPARADOR { get => DIGITO_SEPARADOR; }
        public static string TEST_VERIFICACAO_SEPARADOR { get => VERIFICACAO_SEPARADOR; }
        public static string TEST_TRUE { get => TRUE; }
        public static string TEST_FALSE { get => FALSE; }
        public static string TEST_KEY_COMPLETAR { get => KEY_COMPLETAR; }
        public static string TEST_KEY_VERIFICAR { get => KEY_VERIFICAR; }
        public static string TEST_MATRICULA_INVALIDA { get => MATRICULA_INVALIDA; }


        static void Main(string[] args)
        {
            Console.Clear();
            Console.WriteLine("=== Sistema Gerador e Validador de Matrículas - Empresa XPTO ===");

            string currentDrive = GetCurrentDrive();

            long espacoDisponivelEmDisco = new DriveInfo(currentDrive).AvailableFreeSpace;

            Arquivos arquivos = GetEscolhaDoUsuario();

            if (arquivos.arquivoEntradaExistente)
            {
                if (arquivos.espacoRequerido < espacoDisponivelEmDisco)
                {
                    if (!arquivos.arquivoSaidaReadOnly)
                    {
                        ProcessaArquivos(arquivos);
                    }
                    else
                    {
                        Console.WriteLine("Erro ao processar {0}\nO arquivo de saída {1} está marcado como \"somente leitura\".", arquivos.entrada, arquivos.saida);
                    }
                }
                else
                {
                    Console.WriteLine("Erro ao processar:\nSão necessários {0} bytes de espaço no drive {1} para processar o arquivo {2}",
                        arquivos.espacoRequerido, currentDrive, arquivos.entrada);
                    Console.WriteLine("Libere, ao menos, {0} bytes.", arquivos.espacoRequerido - espacoDisponivelEmDisco);
                }
            }
            else
            {
                Console.WriteLine("Arquivo {0} não encontrado na pasta atual.", arquivos.entrada);
            }
        }

        /*
         * Determina o Tipo de Processamento, ou seja, se o usuário deseja Completar ou Verificar matrículas 
         *  e atribui devidamente o conjunto de arquivos a partir do App.config
         *  Ao final, atribui as propriedades de espaço requerido, read-only etc. 
         */
        private static Arquivos GetEscolhaDoUsuario()
        {
            ConsoleKey input;
            Arquivos conjuntoSelecionado;

            Dictionary<string, Arquivos> PROCESSAMENTO = new Dictionary<string, Arquivos>(){
            { KEY_COMPLETAR,  new Arquivos(
                GetConfigurationString("ARQ_COMPLETAR_ENTRADA"),
                GetConfigurationString("ARQ_COMPLETAR_SAIDA"),
                1.5,
                KEY_COMPLETAR) },

            { KEY_VERIFICAR,  new Arquivos(
                GetConfigurationString("ARQ_VERIFICAR_ENTRADA"),
                GetConfigurationString("ARQ_VERIFICAR_SAIDA"),
                2.5,
                KEY_VERIFICAR) }
            };

            // Aguarda a seleção do usuário para o Tipo de Processamento.
            do
            {
                Console.WriteLine("Digite ({0}) para Completar ou ({1}) para Verificar matrículas:",
                    PROCESSAMENTO.Keys.ElementAt(0), PROCESSAMENTO.Keys.ElementAt(1));
                input = Console.ReadKey(true).Key;
            } while (!(PROCESSAMENTO.Keys.Contains(input.ToString())));


            // Através da key do dicionário ("C" ou "V"), determina o conjunto de arquivos para E/S 
            conjuntoSelecionado = PROCESSAMENTO[input.ToString()];
            SetAtributos(conjuntoSelecionado);
            return conjuntoSelecionado;
        }

        /* 
         * Coleta os atributos como existência dos arquivos de entrada e saída, propriedade read-only
         * e estimativa de espaço necessário para o arquivo de saída.
         * */

        public static void SetAtributos(Arquivos conjuntoSelecionado)
        {
            conjuntoSelecionado.arquivoEntradaExistente = FileExists(conjuntoSelecionado.entrada);
            conjuntoSelecionado.arquivoSaidaExistente = FileExists(conjuntoSelecionado.saida);

            if (conjuntoSelecionado.arquivoEntradaExistente)
            {
                FileInfo fileInfo = new FileInfo(conjuntoSelecionado.entrada);
                conjuntoSelecionado.espacoRequerido = fileInfo.Length * conjuntoSelecionado.proporcaoEntradaSaida;
            }

            if (conjuntoSelecionado.arquivoSaidaExistente)
            {
                conjuntoSelecionado.arquivoSaidaReadOnly = (new FileInfo(conjuntoSelecionado.saida)).IsReadOnly;
                string attr = File.GetAttributes(conjuntoSelecionado.saida).ToString();
            }
        }


        /*
         * Abre e processa a leitura, linha a linha, do arquivo de entrada.
         * Gera ou abre o arquivo de saída e escreve o resultado, linha a linha
         * de acordo com o Tipo de Processamento. Loga resultado exibindo total etc.
         * */
        public static void ProcessaArquivos(Arquivos arquivos)
        {
            int counter = 0;
            string inputLine;

            using (StreamWriter output = new StreamWriter(arquivos.saida))
            {
                StreamReader input = new StreamReader(arquivos.entrada);
                while ((inputLine = input.ReadLine()) != null)
                {
                    inputLine = inputLine.Trim();
                    string resultadoProcessado = ProcessaLinhaDoArquivo(inputLine, arquivos.tipoProcessamento);
                    output.WriteLine(resultadoProcessado);
                    counter++;
                }
                input.Close();
                output.Close();
                Console.WriteLine("\n{0} matrículas processadas do arquivo {1}\nResultados no arquivo {2}", counter, arquivos.entrada, arquivos.saida);
            }
        }

        /**
         * Processa uma linha do arquivo de acordo com o tipoProcessamento.
         * */
        public static string ProcessaLinhaDoArquivo(string inputLine, string tipoProcessamento)
        {
            if (tipoProcessamento.Equals(KEY_COMPLETAR))
            {
                return CompletaMatriculaComDigito(inputLine);
            }
            else
            {
                return GetMatriculaVerdadeiroOuFalso(inputLine);
            }
        }

        /*
         * Processa uma matrícula completa agregando a sua atribuição de verdadeiro ou falso
         * de acordo com a consistência da matrícula com o dígito verificador.
         * */
        public static string GetMatriculaVerdadeiroOuFalso(string matriculaComDigito)
        {
            string testeResult = IsMatriculaConsistenteComDigito(matriculaComDigito) ? TRUE : FALSE;
            return matriculaComDigito + VERIFICACAO_SEPARADOR + testeResult;
        }

        /* 
         * Retorna verdadeiro se uma matrícula for consistente com o seu dígito verificador.
         * */
        public static bool IsMatriculaConsistenteComDigito(string matriculaComDigito)
        {
            string matriculaComDigitoCalculado = CompletaMatriculaComDigito(matriculaComDigito.Split(DIGITO_SEPARADOR.ToCharArray()[0])[0]);
            return matriculaComDigito.Trim().Equals(matriculaComDigitoCalculado);
        }

        /*
         * Dada uma matrícula sem o dígito, retorna a matrícula completa, com o dígito verificador.
         * */
        public static string CompletaMatriculaComDigito(string matriculaSemDigito)
        {
            return matriculaSemDigito + GetSeparadorComDigito(matriculaSemDigito);
        }

        /*
         * Valida a matrícula (se tem 4 digitos todos numéricos). Caso positivo retorna 
         * o separador com o dígito em hexadecimal, caso contrário, retorna 'matrícula inválida'.
         * */
        public static string GetSeparadorComDigito(string matriculaSemDigito)
        {
            if (matriculaSemDigito.Length == 4 && matriculaSemDigito.All(char.IsDigit))
            {
                string digito = GetDigitoVerificador(matriculaSemDigito);
                return DIGITO_SEPARADOR + digito;
            }
            else
            {
                return VERIFICACAO_SEPARADOR + MATRICULA_INVALIDA;
            }
        }

        /*
         * Retorna o dígito verificador em string hexadecimal de acordo com a regra, dada uma matrícula sem o dígito.
         * */
        private static string GetDigitoVerificador(string matriculaSemDigito)
        {
            int sum = 0;
            char[] digitos = matriculaSemDigito.ToCharArray();

            for (int i = 0; i < MULTIPLICADORES.Length; i++)
            {
                sum += Int32.Parse(digitos[i].ToString()) * MULTIPLICADORES[i];
            }
            sum %= DIVISOR;
            string sumToHex = sum.ToString("X");
            return sumToHex;
        }

        /* 
         * Classe para armazenar nomes e propriedades dos arquivos de entrada e saída do sistema.
         * */
        public class Arquivos
        {
            public string entrada, saida;
            public double proporcaoEntradaSaida;
            public double espacoRequerido;
            public string tipoProcessamento;
            public bool arquivoEntradaExistente;
            public bool arquivoSaidaExistente;
            public bool arquivoSaidaReadOnly;

            public Arquivos(string entrada, string saida, double proporcaoEntradaSaida, string tipoProcessamento)
            {
                this.entrada = entrada;
                this.saida = saida;
                this.proporcaoEntradaSaida = proporcaoEntradaSaida;
                this.tipoProcessamento = tipoProcessamento;
            }
        }

        /*
         * Retorna a letra do drive corrente seguida de ":". Ex: "C:"
         * */
        private static string GetCurrentDrive()
        {
            return GetCurrentDirectory().Split('\\')[0];
        }

        /*
         * Retorna o diretório corrente da aplicação. Ex: "c:\users\john\\documentos"
         * */
        private static string GetCurrentDirectory()
        {
            return Directory.GetCurrentDirectory();
        }

        /*
         * Retorna uma string do arquivo de configuração
         * */
        public static string GetConfigurationString(string key)
        {
            return ConfigurationManager.AppSettings.Get(key);
        }

        /*
         * Retorna true se o arquivo especificado na string do parâmetro existe
         * */
        public static bool FileExists(string arquivo)
        {
            return File.Exists(arquivo);
        }

        /*
         * Deleta o arquivo, caso exista
         * */
         public static void FileDelete(string arquivo)
        {
            if (FileExists(arquivo))
            {
                File.Delete(arquivo);
            }
        }
    }
}
