﻿using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace ProcessaMatriculas.test
{
    [TestClass]
    public class ProcessaMatriculasTest
    {
        string DIGITO_SEPARADOR = Program.TEST_DIGITO_SEPARADOR;
        string VERIFICACAO_SEPARADOR = Program.TEST_VERIFICACAO_SEPARADOR;
        string TRUE = Program.TEST_TRUE;
        string FALSE = Program.TEST_FALSE;
        string COMPLETAR = Program.TEST_KEY_COMPLETAR;
        string VERIFICAR = Program.TEST_KEY_VERIFICAR;

        [TestMethod]
        public void Test_CompletaMatriculaComDigito()
        {
            string matricula = "9876";

            string expected = ReplaceDigitoSeparador("9876-E");

            string actual = Program.CompletaMatriculaComDigito(matricula);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Test_IsMatriculaConsistenteComDigito()
        {
            string matriculaComDigito = ReplaceDigitoSeparador("9876-E");

            bool actual = Program.IsMatriculaConsistenteComDigito(matriculaComDigito);
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void Test_ProcessaLinhaDoArquivo()
        {
            string mCompletar = "3849";
            string mCompletarExpected = ReplaceDigitoSeparador("3849-D");

            string mVerificarTrue = ReplaceDigitoSeparador("7240-7");
            string mVerificarTrueExpected = ReplaceDigitoETextoVerificacao("7240-7 verdadeiro");

            string mVerificarFalse = ReplaceDigitoSeparador("6599-E");
            string mVerificarFalseExpected = ReplaceDigitoETextoVerificacao("6599-E falso");

            Assert.AreEqual(mCompletarExpected, Program.ProcessaLinhaDoArquivo(mCompletar, COMPLETAR));
            Assert.AreEqual(mVerificarTrueExpected, Program.ProcessaLinhaDoArquivo(mVerificarTrue, VERIFICAR));
            Assert.AreEqual(mVerificarFalseExpected, Program.ProcessaLinhaDoArquivo(mVerificarFalse, VERIFICAR));
        }

        private string ReplaceDigitoSeparador(string matriculaComDigito)
        {
            return matriculaComDigito.Replace("-", DIGITO_SEPARADOR);
        }

        private string ReplaceDigitoETextoVerificacao(string matriculaComDigitoEVerificacao)
        {
            return ReplaceDigitoSeparador(matriculaComDigitoEVerificacao.Replace(" ", VERIFICACAO_SEPARADOR)
                .Replace("verdadeiro", TRUE)
                .Replace("falso", FALSE));
        }

        [TestMethod]
        // Processa dois arquivos de 10 linhas com matriculas a completar e a verificar.
        // Compara as linhas dos arquivos gerados com os resultados esperados em ambos os casos.
        public void Test_ProcessaArquivos()
        {
            // Conjunto de arquivos para o teste de completar dígitos
            string inputFileCompletar = "testeInputC.txt";
            string outputFileCompletar = "testeOutputC.txt";

            List<string> matriculasSemDigito = new List<string>() { "0218", "2502", "2907", "3429", "3849", "4116", "4307", "5896", "7038", "8321" };
            List<string> matriculasComDigitoExpected = new List<string>() { "0218-B", "2502-2", "2907-C", "3429-7", "3849-D", "4116-7", "4307-E", "5896-0", "7038-C", "8321-C" };


            // Gera o arquivo de entrada de matrículas sem o dígito.
            StreamWriter swInputCompletar = new StreamWriter(inputFileCompletar);
            matriculasSemDigito.ForEach(
                delegate (string matricula)
                {
                    swInputCompletar.WriteLine(matricula);
                });

            swInputCompletar.Close();

            Program.Arquivos arquivosCompletar = new Program.Arquivos(inputFileCompletar, outputFileCompletar, 1.5, "C");

            Program.SetAtributos(arquivosCompletar);

            // Processa as matrículas do arquivo de entrada, gerando o arquivo de saída.
            Program.ProcessaArquivos(arquivosCompletar);

            string inputLine;
            int countLine = 0;

            // Compara linha a linha do arquivo de saída com a saída esperada
            StreamReader input = new StreamReader(arquivosCompletar.saida);
            while ((inputLine = input.ReadLine()) != null)
            {
                Assert.AreEqual(ReplaceDigitoSeparador(matriculasComDigitoExpected[countLine]), inputLine);
                countLine++;
            }
            input.Close();

            // Elimina os arquivos do disco
            Program.FileDelete(inputFileCompletar);
            Program.FileDelete(outputFileCompletar);

            // Conjunto de arquivos para o teste de verificar dígitos
            string inputFileVerificar = "testeInputV.txt";
            string outputFileVerivicar = "testeOutputV.txt";

            List<string> matriculasVerificarComDigito = new List<string>() { "0816-E", "2315-3", "4166-6", "5830-2", "6599-E", "6668-8", "7240-7", "7778-4", "7981-1", "9203-A" };
            List<string> matriculasVerificarVerificacaoExpected = new List<string>() { "0816-E falso", "2315-3 verdadeiro", "4166-6 verdadeiro", "5830-2 verdadeiro", "6599-E falso", "6668-8 verdadeiro", "7240-7 verdadeiro", "7778-4 verdadeiro", "7981-1 verdadeiro", "9203-A falso" };


            Program.Arquivos arquivosVerificar = new Program.Arquivos(inputFileVerificar, outputFileVerivicar, 2.5, "V");

            Program.SetAtributos(arquivosVerificar);

            // Gera o arquivo de entrada de matrículas sem o dígito.
            StreamWriter swInputVerificar = new StreamWriter(inputFileVerificar);
            matriculasVerificarComDigito.ForEach(
                delegate (string matriculaComDigito)
                {
                    swInputVerificar.WriteLine(ReplaceDigitoSeparador(matriculaComDigito));
                });

            swInputVerificar.Close();
            // Processa as matrículas do arquivo de entrada, gerando o arquivo de saída.
            Program.ProcessaArquivos(arquivosVerificar);

            countLine = 0;

            // Compara linha a linha do arquivo de saída com a saída esperada
            input = new StreamReader(arquivosVerificar.saida);
            while ((inputLine = input.ReadLine()) != null)
            {
                Assert.AreEqual(ReplaceDigitoETextoVerificacao(matriculasVerificarVerificacaoExpected[countLine]), inputLine);
                countLine++;
            }
            input.Close();

            // Elimina os arquivos do disco
            Program.FileDelete(inputFileVerificar);
            Program.FileDelete(outputFileVerivicar);

        }
    }
}
